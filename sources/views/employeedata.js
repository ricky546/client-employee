import { JetView } from "webix-jet";
import { data } from "models/records";

export default class EmployeeData extends JetView {
	config() {
		return {
			view: "datatable",
			css: "webix_shadow_medium",
			select:true,
			columns: [
				{id:"edit", header:"", width:90, template:"<input type='button' class='webix_button edit_button' value='Edit'></span>"},
				{id:"delete", header:"", width:90, template:"<input type='button' class='webix_button delete_button' value='Remove'></span>"},
				{
					id: "_id", header: ["id", { content: "textFilter" }],
					sort: "text", fillspace: true
				},
				{id:"fullname", header:"Full Name", sort:"text", adjust:"data"},
				{id:"phone_number", header:"Phone Number", sort:"text", width:120},
				{id:"salary", header:"Salary", sort:"text", width:200}
			],
			// onClick:{
			// 	"edit_button":(e, id) => {
			// 		let item = this.getRoot().getItem(id)
			// 		this.show("employee?id="+ item._id)
			// 	}
			// },
			onClick:{
				"delete_button":(e, id) => {
					let item = this.getRoot().getItem(id)
					webix.confirm({
						text:"The employee will be deleted. <br/> are you sure ?",
						ok:"Yes", cancel:"Cancel",
						callback:(res) => {
							if(res){
								webix.ajax().del('https://employee-restapi.herokuapp.com/employees/Delete/'+ item._id)
							}
						}
					})
				}
			}
		};
	}
	init(view) {
		view.parse(data)
	}
}