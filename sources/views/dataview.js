import { JetView } from "webix-jet";
import EmployeeData from "views/employeedata"
import EmployeeForm from "views/forms/employeeform"

export default class DataView extends JetView {
  config() {
    return {
      rows: [
        {
          view: "toolbar", css: "subbar", padding: 0,
          elements: [
            {
              height: 50, css: "title", borderless: true,
              template: '<div class="header">Employees</div><div class="details">(info & editing)</div>'
            },
            {
              view: "button", id: "button:add", type: "iconButton",
              icon: "plus", label: "Add customer", width: 140,
              click: () => {
                this.$$("multi").setValue("formView")
              }
            }
          ]
        },
        {
          animate: false,
          fitBiggest: true,
          localId: "multi",
          cells: [
            { id: "gridView", $subview: EmployeeData },
            { id: "formView", $subview: EmployeeForm }
          ],
          on: {
            onViewChange: (prev) => {
              const button = this.$$("button:add")
              if (prev == "gridView")
                button.hide()
              else
                button.show()
            }
          }
        }
      ]
    }
  }
}