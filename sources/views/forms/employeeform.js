import { JetView } from "webix-jet"

export default class EmployeeForm extends JetView {
  config() {
    return {
      view: "form", paddingY: 20, paddingX: 30,
      elementsConfig: { labelWidth: 100 },
      elements: [
        { type: "header", height: 45, template: "Employee's info editor" },
        { view: "text", name: "fullname", label: "fullname" },
        { view: "text", name: "phone_number", label: "phone_number" },
        { view: "text", name: "salary", label: "salary" },
        {
          margin: 10,
          cols: [
            {
              view: "button", value: "<< Back", align: "center", width: 120,
              click: () => {
                this.getBack()
              }
            },
            {
              view: "button", value: "Save", type: "form", align: "center", width: 120,
              click: () => {
                const form = this.getRoot()
                if(form.validate()) {
                  this.saveData(form.getValues())
                  this.getBack()
                }
              }
            }
          ]
        }
      ]
    }
  }
  // urlChange(form) {
  //   data.waitData.then(() => {
  //     const id = this.getParam("id")

  //     if (id && data.exists(id)) {
  //       form.setValues(data.getItem(id))
  //     }
  //   })
  // }
  saveData(values) {
    webix.ajax().headers({
      "Content-type":"application/json"
    }).post('https://employee-restapi.herokuapp.com/employees/Add', values)
  }
  getBack() {
    const form = this.getRoot()
    form.clear()
    form.clearValidation()
    this.show("start")
  }
}
